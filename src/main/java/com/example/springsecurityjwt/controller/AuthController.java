package com.example.springsecurityjwt.controller;

import com.example.springsecurityjwt.payload.request.LoginReuqest;
import com.example.springsecurityjwt.payload.request.RegistreRequest;
import com.example.springsecurityjwt.service.AuthService;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {


    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginReuqest loginReuqest) {
        return ResponseEntity.ok(authService.login(loginReuqest));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegistreRequest request){
        return ResponseEntity.ok(authService.registerUser(request));
    }

}
