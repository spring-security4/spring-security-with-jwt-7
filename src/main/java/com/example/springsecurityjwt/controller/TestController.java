package com.example.springsecurityjwt.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    /**
     * Asagida 3 qrup method var bunlar test1 test2 ve test3 ile basliyan metodlardir
     * biz 4 dene role yaradacayiq ve bu role lara gore metodlardaki accesslerini bildireceyik
     * misal
     *
     * USER role yalniz  1ci qrupdan test-read access i ne sahib olacaq
     *
     * MANAGER role ise  1ci qrupdan test-read , test-write, test-update, test-delete
     *                   2ci qrupdan test2-read, test2-write, test2-update
     *                   3cu qrupdan test3-read  e accessi olacaq yalniz
     *
     * SUPERVISOR role   1ci qrupdan test-read , test-write, test-update, test-delete
     *                   2ci qrupdan test2-read, test2-write, test2-update,test2-delete
     *                   3cu qrupdan test3-read, test3-write  a accessi olacaq
     *
     * ADMIN ise  butun metodlara accessi olacaq
     *
     * bazada role ve permission lari ele yazmaq lazimdirki eyni datalar tekrarlanmasin bununucun bazada statik
     * ROLE ve PERMISSION lar yazacayiq ve id elerine gore teyin etmeye calisacayiq
     *
     * configuration da httpBasic yazdigimiz ucun postmanda sorgu atarken Auth bolmesinden username ve passwordu daxil edeceksiz her hansi methodu cagirarken
     * diger metodlarda artiq lazim olmayacaq  bir defe auth olmaniz yeterlidi
     *
     * ve hansi userin neye accessi varsa o metodlari cagira bilecek
     *
     * register olmaq ucun auth a ehtiyac yoxdu , Register olduqda yeni user yaranir ve default olaraq ROLE_USER ve yalniz test1-read permission ni verilir
     * sonrada eger adminsinizse role ve permissionlarini deyisib istediyiniz accessi vere bilersiz
     * UpdateRolePermissionController classindan  /give-access/{id} url ne muraciet ederek istediyiniz usere access vere bileceksiz
     * ancaq admin deyilsinizse bu accessi vere bilme imkaniniz yoxdu
     * security baximindan ilk admini bazada ozumuz yaradiriq ve butun permissionlari veririk ve hemin admin digerlerinede access
     *  vere bilir
     */


    /**
     * JWT Token qosdugumuz ucun artiq authorize lar bu token sayesinde olacaq buna gore postmanda request atarken header
     * hissesine login olduqda aldigimiz tokeni header value suna yazmaliyiq beleikle role ve permissionlara uygun olaraq
     * hemin metodlara accessi miz olacaq
     */
    //  =====================  TEST 1 METHODS =====================

    @GetMapping("/test1-all")
    @PreAuthorize("hasAuthority('TEST1-READ')")
    public String getAll() {
        return "test get all ";
    }

    @PostMapping("/test1-create")
    @PreAuthorize("hasAuthority('TEST1-WRITE')")
    public String create() {
        return "test create";
    }

    @PutMapping("/test1-update")
    @PreAuthorize("hasAuthority('TEST1-UPDATE')")
    public String update() {
        return "test update";
    }

    @DeleteMapping("/test1-delete")
    @PreAuthorize("hasAuthority('TEST1-DELETE')")
    public String delete() {
        return "test delete";
    }

    //  =====================  TEST 2 METHODS =====================

    @GetMapping("/test2-all")
    @PreAuthorize("hasAuthority('TEST2-READ')")
    public String getTest2All() {
        return "test2 all ";
    }

    @PostMapping("/test2-create")
    @PreAuthorize("hasAuthority('TEST2-WRITE')")
    public String createTest2() {
        return "test2 create";
    }

    @PutMapping("/test2-update")
    @PreAuthorize("hasAuthority('TEST2-UPDATE')")
    public String updateTest2() {
        return "test2 update";
    }

    @DeleteMapping("/test2-delete")
    @PreAuthorize("hasAuthority('TEST2-DELETE')")
    public String deleteTest2() {
        return "test2 delete";
    }

    //  =====================  TEST 3 METHODS =====================

    @GetMapping("/test3-all")
    @PreAuthorize("hasAuthority('TEST3-READ')")
    public String getTest3All() {
        return "test3 all ";
    }

    @PostMapping("/test3-crete")
    @PreAuthorize("hasAuthority('TEST3-WRITE')")
    public String createTest3() {
        return "test3 create";
    }

    @PutMapping("/test3-update")
    @PreAuthorize("hasAuthority('TEST3-UPDATE')")
    public String updateTest3() {
        return "test3 update";
    }

    @DeleteMapping("/test3-delete")
    @PreAuthorize("hasAuthority('TEST3-DELETE')")
    public String deleteTest3() {
        return "test3 delete";
    }
}
