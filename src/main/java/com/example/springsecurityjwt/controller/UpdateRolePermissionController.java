package com.example.springsecurityjwt.controller;

import com.example.springsecurityjwt.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt.service.MyUserService;
import com.google.common.net.HttpHeaders;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UpdateRolePermissionController {

    /**
     * Role id ni vermekle biz ona uygun permessionlari vermis olacayiq yeni buna gore ayri ayriliqda permissionlari qeyd etmeye ehtiyac yxodur
     * Eger her user ucun xusui role ve permission yazmaq istesek bunun ucun bazada deyisiklik etmeliyik
     * artiq table mizda user_id role_id ve permission_id bir table da olmalidiki her usere uygun role ve permissionlar olsun
     */
    private final MyUserService myUserService;

    @PutMapping("/give-access/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String updateRolePermission(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @PathVariable Long id, @RequestBody UpdateRolePermissionRequest permissionRequest) {

        return myUserService.updateUserAccessRolePermission(token,id, permissionRequest);
    }
}
