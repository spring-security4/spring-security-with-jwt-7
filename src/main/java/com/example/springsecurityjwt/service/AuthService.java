package com.example.springsecurityjwt.service;

import com.example.springsecurityjwt.jwt.JwtUtils;
import com.example.springsecurityjwt.model.MyUser;
import com.example.springsecurityjwt.model.Permission;
import com.example.springsecurityjwt.model.Role;
import com.example.springsecurityjwt.payload.request.LoginReuqest;
import com.example.springsecurityjwt.payload.request.RegistreRequest;
import com.example.springsecurityjwt.payload.response.JwtResponse;
import com.example.springsecurityjwt.payload.response.RegisterResponse;
import com.example.springsecurityjwt.repository.MyUserRepository;
import com.example.springsecurityjwt.repository.PermissioRepository;
import com.example.springsecurityjwt.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {

    private static final Integer ROLE_USER = 4;
    private static final Integer READ_TEST1 = 1;

    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final MyUserRepository myUserRepository;
    private final RoleRepository roleRepository;
    private final PermissioRepository permissioRepository;

    private final JwtUtils jwtUtils;

    public JwtResponse login(LoginReuqest loginReuqest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginReuqest.getUsername(), loginReuqest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtils.generateToken(authentication);
        return new JwtResponse(token);
    }

    public RegisterResponse registerUser(RegistreRequest request){
        MyUser myUser = new MyUser();
        myUser.setUsername(request.getUsername());
        myUser.setPassword(passwordEncoder.encode(request.getPassword()));
        myUser.setEmail(request.getEmail());
        myUser.setFirstName(request.getFirstName());
        myUser.setLastName(request.getLastName());
        myUser.setCreatedDate(LocalDateTime.now());
        myUser.setRole(defaultRole());
        myUserRepository.save(myUser);
        return new RegisterResponse("SUCCESS");
    }

    public Role defaultRole() {
        Optional<Role> role = roleRepository.findById(ROLE_USER);
        Permission permissions = permissioRepository.getById(READ_TEST1);
        role.get().setPermissions(new ArrayList<>(Collections.singletonList(permissions)));
        return role.get();
    }
}
