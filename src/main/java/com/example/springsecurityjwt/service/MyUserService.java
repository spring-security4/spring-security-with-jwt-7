package com.example.springsecurityjwt.service;

import com.example.springsecurityjwt.dto.RegisterRequest;
import com.example.springsecurityjwt.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt.jwt.JwtUtils;
import com.example.springsecurityjwt.model.MyUser;
import com.example.springsecurityjwt.model.Permission;
import com.example.springsecurityjwt.model.Role;
import com.example.springsecurityjwt.repository.MyUserRepository;
import com.example.springsecurityjwt.repository.PermissioRepository;
import com.example.springsecurityjwt.repository.RoleRepository;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyUserService {

    private final MyUserRepository myUserRepository;
    private final RoleRepository roleRepository;

    public String updateUserAccessRolePermission(String token, Long id, UpdateRolePermissionRequest permissionRequest) {
            MyUser myUser = myUserRepository.getById(id);
            myUser.setRole(roleRepository.getById(permissionRequest.getRoleId()));
            myUserRepository.save(myUser);
            return "SUCCESS";

    }

}
