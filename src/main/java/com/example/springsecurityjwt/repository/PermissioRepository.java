package com.example.springsecurityjwt.repository;

import com.example.springsecurityjwt.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissioRepository extends JpaRepository<Permission,Integer> {
}
