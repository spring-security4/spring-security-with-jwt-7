package com.example.springsecurityjwt.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistreRequest {

    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
}
