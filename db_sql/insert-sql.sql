
insert into public.role (id, role_name)
values  (1, 'ADMIN'),
        (2, 'SUPERVISOR'),
        (3, 'MANAGER'),
        (4, 'USER');


insert into public.permission (id, permission_name)
values  (1, 'TEST1-READ'),
        (2, 'TEST1-WRITE'),
        (3, 'TEST1-UPDATE'),
        (4, 'TEST1-DELETE'),
        (5, 'TEST2-READ'),
        (6, 'TEST2-WRITE'),
        (7, 'TEST2-UPDATE'),
        (8, 'TEST2-DELETE'),
        (9, 'TEST3-READ'),
        (10, 'TEST3-WRITE'),
        (11, 'TEST3-UPDATE'),
        (12, 'TEST3-DELETE');



insert into public.role_permissions (role_id, permission_id)
values  (1, 1),
        (1, 2),
        (1, 3),
        (1, 4),
        (1, 5),
        (1, 6),
        (1, 7),
        (1, 8),
        (1, 9),
        (1, 10),
        (1, 11),
        (1, 12),
        (2, 1),
        (2, 2),
        (2, 3),
        (2, 4),
        (2, 5),
        (2, 6),
        (2, 7),
        (2, 8),
        (2, 9),
        (2, 10),
        (4, 1),
        (3, 1),
        (3, 2),
        (3, 3),
        (3, 5),
        (3, 6),
        (3, 4),
        (3, 7);


